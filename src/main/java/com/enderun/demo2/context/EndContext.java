package com.enderun.demo2.context;

import com.enderun.demo2.annotation.EndBean;
import com.enderun.demo2.bean.UserBean;
import org.apache.commons.lang3.text.WordUtils;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EndContext extends Context {

    private boolean initialized = false;

    private Map<String, Class> beandefs = new HashMap<>();

    private Map<String, Object> singletons = new HashMap<>();

    private Map<String, Object> sessions = new HashMap<>();
    private Map<String, Object> requests = new HashMap<>();


    private static EndContext ourInstance = new EndContext();

    public static EndContext getInstance() {
        return ourInstance;
    }

    private EndContext() {
//        super();

    }

    public void init() {
        if(initialized) {
            return;
        }
        initialized = true;
        Reflections reflections = new Reflections(ClasspathHelper.forPackage("com.enderun"),
                new SubTypesScanner(),
                new MethodAnnotationsScanner(),
                new TypeAnnotationsScanner()
        );
        Set<Class<?>> beanTypeSet = reflections.getTypesAnnotatedWith(EndBean.class);
        for (Class<?> beanType : beanTypeSet) {
            String beanName = getBeanName(beanType);
            beandefs.put(beanName, beanType);
        }
    }

    private String getBeanName(Class beanType) {
        EndBean ann = (EndBean) beanType.getAnnotation(EndBean.class);
        String beanName = ann.value();
        if(beanName.equals("")) {
            beanName = beanType.getSimpleName();
        }
        return beanName;
    }

    public <T> T getBean(Class<T> beanType) {
        return (T) getBean(getBeanName(beanType));
    }

    public Object getBean(String beanName) {
        Object bean = beandefs.get(beanName);
        if(bean == null) {
            synchronized (beandefs) {
                if(bean == null) {
                    Class beanType = beandefs.get(beanName);
                    try {
                        bean = beanType.newInstance();
                        beandefs.put(beanName, bean.getClass());
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return bean;
    }

}
