package com.enderun.demo2.servlet;

import com.enderun.demo2.bean.UserBean;
import com.enderun.demo2.context.EndContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;


@WebServlet(name = "view", urlPatterns = "/view", loadOnStartup = 0)
public class EndServlet extends HttpServlet {


    @Override
    public void init() throws ServletException {
        super.init();
        EndContext.getInstance().init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserBean userBean = (UserBean)EndContext.getInstance().getBean("userBean");
        UserBean userBean2 = EndContext.getInstance().getBean(UserBean.class);
        System.out.println(userBean);
        System.out.println(userBean2);
        System.out.println(userBean.getHello());
        resp.getWriter().write(userBean.getHello());
    }
}
