package com.enderun.demo2.bean;


import com.enderun.demo2.annotation.EndBean;

@EndBean("userBean")
public class UserBean {

    private String hello = null;

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }
}
